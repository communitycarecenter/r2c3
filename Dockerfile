FROM alpine

MAINTAINER Alessandro Surace "zioalex@gmail.com"

#RUN apt-get update -y && \
#    apt-get install -y python-pip python-dev

RUN apk update && apk add python3 py3-virtualenv
# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app/r2c3

COPY . /app/r2c3

RUN virtualenv -p `which python3` r2c3_venv
RUN . r2c3_venv/bin/activate
RUN pip3 install -r requirements.txt
RUN python3 setup.py install


ENTRYPOINT [ "python3" ]

CMD [ "r2c3/app.py" ]
