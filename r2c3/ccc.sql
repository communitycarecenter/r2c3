CREATE TABLE patient (
        id INTEGER NOT NULL, 
        name VARCHAR(40), 
        address VARCHAR(60),
        city VARCHAR(40),
        zipcode INTEGER(5),
        PRIMARY KEY (id)
);

CREATE TABLE data (
        id INTEGER NOT NULL, 
        patient_id INTEGER,
        location VARCHAR(40), 
        timestamp DATETIME, 
        age INTEGER,
        symptoms TEXT, 
        oximetry INTEGER, 
        ethnicity VARCHAR(40), 
        bmi INTEGER[2], 
        PRIMARY KEY (id)
        FOREIGN KEY(patient_id) REFERENCES patient(id)
);

