from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def reset_database():
    from r2c3.database.models_ccc import Data, Patient  # noqa
    db.drop_all()
    db.create_all()
