# The examples in this file come from the Flask-SQLAlchemy documentation
# For more information take a look at:
# http://flask-sqlalchemy.pocoo.org/2.1/quickstart/#simple-relationships

from datetime import datetime

from r2c3.database import db


class Patient(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    address = db.Column(db.String(60))
    city = db.Column(db.String(40))
    zipcode = db.Column(db.Integer)

    def __init__(self, name, address, city, zipcode):
        self.name = name
        self.address = address
        self.city = city
        self.zipcode = zipcode

    def __repr__(self):
        return '<Patient %r created>' % self.name

class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # location = db.Column(db.String(40))
    location = db.Column(db.String(40))
    timestamp = db.Column(db.DateTime)
    age = db.Column(db.Integer)
    symptoms = db.Column(db.String(80))
    oximetry = db.Column(db.Integer)
    ethnicity = db.Column(db.String(40))
    bmi = db.Column(db.Integer)
    data = db.relationship('Patient', backref=db.backref('data', lazy=True))
    patient_id = db.Column(db.Integer, db.ForeignKey('patient.id'))

    def __init__(self, location, timestamp, age, symptoms, oximetry, ethnicity, bmi, patient_id):
        self.location = location
        if timestamp is None:
            timestamp = datetime.utcnow()
        self.timestamp = timestamp
        self.age = age
        self.symptoms = symptoms
        self.oximetry = oximetry
        self.ethnicity = ethnicity
        self.bmi = bmi
        self.patient_id = patient_id

    def __repr__(self):
        return '<Patient Event %r created>' % id
