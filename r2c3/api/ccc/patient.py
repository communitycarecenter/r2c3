from r2c3.database import db
from r2c3.database.models_ccc import Patient, Data

def create_patient(data):
    name = data.get('name')
    address = data.get('address')
    city = data.get('city')
    zipcode = data.get('zipcode')
    patient = Patient(name, address, city, zipcode)
    db.session.add(patient)
    db.session.commit()


def update_patient(patient_id, data):
    patient = Patient.query.filter(Patient.id == patient_id).one()
    patient.name = data.get('name')
    patient.address = data.get('address')
    patient.city = data.get('city')
    db.session.add(patient)
    db.session.commit()


def delete_patient(id):
    patient = Patient.query.filter(Patient.id == id).one()
    db.session.delete(patient)
    db.session.commit()


def create_data(data):
    location = data.get('location')
    timestamp = data.get('timestamp')
    age = data.get('age')
    symptoms = data.get('symptoms')
    oximetry = data.get('oximetry')
    ethnicity = data.get('ethnicity')
    bmi = data.get('bmi')
    patient_id = data.get('patient_id')

    data = Data(location, timestamp, age, symptoms, oximetry, ethnicity, bmi, patient_id)

    db.session.add(data)
    db.session.commit()


def update_data(patient_id, data):
    event = Data.query.filter(Data.patient_id == patient_id).one()
    event.location = data.get('location')
    event.timestamp = data.get('timestamp')
    event.age = data.get('age')
    event.symptoms = data.get('symptoms')
    event.oximetry = data.get('oximetry')
    event.ethnicity = data.get('ethnicity')
    event.bmi = data.get('bmi')
    event.patient_id = data.get('patient_id')

    db.session.add(event)
    db.session.commit()


def delete_data(event_id):
    event = Data.query.filter(Data.id == event_id).one()
    db.session.delete(event)
    db.session.commit()
