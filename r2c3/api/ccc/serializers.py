from flask_restplus import fields
from r2c3.api.restplus import api

patient = api.model('Patient Onboard', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of the patient'),
    'name': fields.String(required=True, description='Patient name'),
    'address': fields.String(required=True, description='Address'),
    'city': fields.String(required=True, description='City'),
    'zipcode': fields.Integer(required=True, description='Zipcode'),
})

data = api.model('Patient Data', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of the data record'),
    'location': fields.String(required=True, description='Patient location - GPS coordinates'),
    # 'timestamp': fields.DateTime,
    'age': fields.Integer(required=True, description='Patient age'),
    'symptoms': fields.String(required=True, description='Observed symptoms'),
    'oximetry': fields.Integer(required=True, description='Patient oximetry', default=0),
    'ethnicity': fields.String(required=True, description='Patient ethnicity'),
    'bmi': fields.Integer(required=True, description='BMI'),
    'patient_id': fields.Integer(required=True, description='Patient ID'),

 })
