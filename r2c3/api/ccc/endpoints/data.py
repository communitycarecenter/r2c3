import logging

from flask import request
from flask_restplus import Resource
from r2c3.api.restplus import api

# CCC
from r2c3.api.ccc.patient import create_data, update_data, delete_data
from r2c3.api.ccc.serializers import patient, data  # Done
from r2c3.api.restplus import api
from r2c3.database.models_ccc import Patient, Data  # Done

log = logging.getLogger(__name__)

ns = api.namespace('patient/data', description='Operations related to add events for patients')


@ns.route('/')
class DataCollection(Resource):

    @api.marshal_list_with(data)
    def get(self):
        """
        Returns list of data events
        """
        events = Data.query.all()
        return events

    @api.response(201, 'Data event successfully created.')
    @api.expect(data)
    def post(self):
        """
        Creates a new data event
        """
        data = request.json
        create_data(data)
        return None, 201


@ns.route('/<int:id>')
@api.response(404, 'Patient ID not found.')
class PatientEvents(Resource):

    @api.marshal_with(data)
    def get(self, id):
        """
        Returns a list of event x Patient
        """
        print("DEBUG ID:", id)
        return Data.query.filter(Data.patient_id == id).all()

    @api.expect(data)
    @api.response(204, 'Data event successfully updated.')
    def put(self, id):
        """
        Updates a data event record.

        Use this method to update any data event.

        * Send a JSON object with the new name in the request body.

        ```
       {
          "age": 58,
          "bmi": 23,
          "ethnicity": "Europeans",
          "location": "Some where",
          "oximetry": 98,
          "patient_id": 1,
          "symptoms": "Headcache"
        }
        ```

        * Specify the patient_id of the patient to modify in the request URL path.
        """
        data = request.json
        update_data(id, data)
        return None, 204

    @api.response(204, 'Event successfully deleted.')
    def delete(self, id):
        """
        Deletes a data event
        """
        delete_data(id)
        return None, 204
