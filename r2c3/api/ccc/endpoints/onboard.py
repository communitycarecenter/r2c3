import logging

from flask import request
from flask_restplus import Resource
from r2c3.api.restplus import api


# CCC
from r2c3.api.ccc.patient import create_patient, delete_patient, update_patient, create_data, update_data, delete_data
from r2c3.api.ccc.serializers import patient, data  # Done
from r2c3.api.restplus import api
from r2c3.database.models_ccc import Patient, Data  # Done


log = logging.getLogger(__name__)

ns = api.namespace('onboard/patient', description='Operations related to onboard a new patient')


@ns.route('/')
class PatientCollection(Resource):

    @api.marshal_list_with(patient)
    def get(self):
        """
        Returns list of patients
        """
        patients = Patient.query.all()
        return patients

    @api.response(201, 'Patient successfully created.')
    @api.expect(patient)
    def post(self):
        """
        Creates a Patient
        """
        data = request.json
        print(request.json)
        create_patient(data)
        return None, 201


@ns.route('/<int:id>')
@api.response(404, 'Patient id not found.')
class PatientId(Resource):

    @api.marshal_with(patient)
    def get(self, id):
        """
        Returns a spaecif Patient Id
        """
        return Patient.query.filter(Patient.id == id).one()

    @api.expect(patient)
    @api.response(204, 'Patient successfully updated.')
    def put(self, id):
        """
        Updates a Patient

        Use this method to change the patient data.

        * Send a JSON object with the new name in the request body.

        ```
        {
          "id": 1
          "address": "InvalidenStr 10",
          "city": "Berlin",
          "name": "John Smith",
          "zipcode": 10100
        }
        ```

        * Specify the ID of the patient to modify in the request URL path.
        """
        data = request.json
        update_patient(id, data)
        return None, 204

    @api.response(204, 'Patient successfully deleted.')
    def delete(self, id):
        """
        Deletes Patient Id
        """
        delete_patient(id)
        return None, 204
