r2c3
=============

This repository contains a backbone API design to support the Rapid Response Community Care Center idea as outlined [here](https://catalyst.nejm.org/doi/full/10.1056/CAT.20.0080).

This code has been developed during the [CodeVsCOVID19 hackathon](https://www.codevscovid19.org/)

The Original boilerplate code can be found [here](https://github.com/postrational/rest_api_demo)

How to start to use it
======================
Download the code in your workspace

    $ cd /path/to/my/workspace/
    $ git clone https://gitlab.com/communitycarecenter/r2c3
    $ cd r2c3

Create a Python Virtualenv and activate it

    $ virtualenv -p `which python` r2c3
    $ source r2c3/bin/activate
    (r2c3) $ pip install -r requirements.txt

Setup the development environment and start it:

    (r2c3) $ python setup.py develop
    (r2c3) $ python r2c3/app.py

Open the browser to http://localhost:8000/api/

Docker Container
================
Build the container

    docker build -t r2c3:latest .

Run it

    docker run -d -p 8000:8000 r2c3

Tag

    docker tag r2c3 eu.gcr.io/r2c3-codevscovid19-hackathon/r2c3

Push to Google Registry

    docker push eu.gcr.io/r2c3-codevscovid19-hackathon/r2c3


DB reset
========
We provide an pre-filled Example sqlite3 DB
To clean-up it and start from scratch just execute

    rm db.sqlite
    sqlite3 db.sqlite < ccc.sql
